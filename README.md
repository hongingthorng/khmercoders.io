# KhmerCoders Website

Build status for master branch: [![build status](https://gitlab.com/khmercoders/khmercoders.io/badges/master/build.svg)](https://gitlab.com/khmercoders/khmercoders.io/commits/master)

---

This is repository for our website. Your contribution will directly deploy to our website.

#### What is KhmerCoders?

Khmer Coders is the biggest Cambodian programmers community. We love each other, we help each other, and we are passionate about new technology. [Join us here](https://www.facebook.com/groups/1104437376352783/) or visit our website [khmercoders.io](https://khmercoders.io).

#### How to contribute?

*Coming Soon*