<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index()
    {
        return view('member', array(
            "users" => User::get()
        ));
    }
}
