<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\ResourceContributor;

class ResourceController extends Controller
{
    public function index()
    {
        return view(
            "resource", 
            array(
                "resources" => Resource::with('contributors')->orderBy("id", "desc")->get()
            )
        );
    }
}
