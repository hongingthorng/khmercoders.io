<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ResourceContributor extends Model
{
    protected $table = 'resource_contributors';

    public static function getContributorByResourceID($resource_id) {
        return DB::table('resource_contributors')
            ->join('users', 'users.id', 'resource_contributors.user_id')
            ->where('resource_id', $resource_id)
            ->select('name', 'display_name', 'position_name', 'picture', 'job_name')
            ->get();
    }
}
