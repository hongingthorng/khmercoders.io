@extends('master')
@section('title', 'Khmer Coders Registration')

@section('content')

<div class="container py-5">

    <h1 class='h3 mb-5'>Registration</h1>

    <form method="POST" action="{{ route('register') }}" style='max-width:500px;'>
        {{ csrf_field() }}

        <div>
            <label for="name">Display Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class='my-3'>
            <label for="email">Email</label>

            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class='my-3'>
            <label for="password">Password</label>

            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class='my-3'>
            <label for="password-confirm">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>

        <button type="submit" class="btn btn-primary my-3">
            Register
        </button>

    </form>
</div>
@endsection
