@extends('master')
@section('title', 'Edit Profile')


@section('content')

    <div class='container my-5'>
        <h1 class='h4'>Edit your profile</h1>
        <hr>
 
        <div style='margin: 50px 0px; max-width: 500px;'>    
            
            {{ Form::open( array ('url' => '/profile/update', 'method' => 'post')) }} 
                
                {{ Form::label ('title', 'Display Name:' )  }}
                {{ Form::text('display_name', $user->display_name, ['class' => 'form-control' ]) }}
                
                {{ Form::label('title', 'Job Position:') }}
                {{ Form::text('job_name', $user->job_name, ['class' => 'form-control']) }}
                
                <br>
                
                {{ Form::submit('Save',['class'=>'btn btn-success']) }}

            {{ Form::close() }}
                       
        </div>

    </div>

@endsection
