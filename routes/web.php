<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/resources', 'ResourceController@index');
Route::get('/members', 'MemberController@index');

Route::get('/dashboard', 'DashboardController@index');

// Handle profile
Route::post('/profile/update', 'ProfileController@update');
Route::get('/profile/edit', 'ProfileController@edit');

Route::resource('users', 'UsersController');



Auth::routes();
